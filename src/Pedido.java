import java.net.PortUnreachableException;
import java.util.Date;

/**
 * Clase Pedido
 *
 * Contiene la informacion de los pedidos que se realizarán
 *
 * @author XG3T2
 * @version 1.0
 * @since 28/02/2022
 */
public class Pedido {

    /**
     * Importe total del pedido
     */
    Double importeTotal;
    /**
     * Estado actual del pedido
     * @var String
     */
    String estado;
    Cliente cliente;
    Producto producto;
    Producto producto2;
    /**
     * Cantidad disponible del producto
     */
    double codigo;


    public Pedido(){
        this.importeTotal=0.00;
        this.estado="";
        this.cliente=null;
        this.producto=null;
        this.producto2=null;
        this.codigo=0;
    }



    //METODOS

    /**
     * Agrega un cliente al pedido
     *
     * @param cliente cliente que realiza el pedido
     */
    public void agregarCliente(Cliente cliente){

        this.cliente=cliente;
    }

    /**
     * Agrega el producto al cliente
     * @param producto producto que se añade al pedido
     */
    public void agregarProducto(Producto producto){

        this.producto=producto;

    }

    /**
     * Agrega un segundo producto al pedido
     * @param producto producto que se añade al pedido
     */
    public void agregarProducto2(Producto producto){


        this.producto2=producto;

    }

    /**
     * Elimina el primer producto del pedido
     */
    public void eliminarProducto(){

        this.producto=null;

    }

/**
 * Elimina el segundo producto del pedido
 */
    public void eliminarProducto2(){


        this.producto2=null;

    }

    /**
     * Muestra la información de que hay en el pedido actual
     */
    public void mostrarLista(){

        System.out.println("CANT         "+"PRODUCTO         "+"PRECIO UNIDAD         "+ "TOTAL");
        System.out.println("====         "+"========         "+ "============         "+ "=======");

        if (producto != null) {
            System.out.println(this.producto.cantidad + "              " + this.producto.nombre + "      " +
                    "            " + producto.precio + "              "+this.producto.cantidad*this.producto.precio);
        }
        if (producto2 != null){
            System.out.println(this.producto2.cantidad + "              " + this.producto2.nombre + "      " +
                    "            " + this.producto2.precio + "         "+this.producto2.cantidad*this.producto2.precio);
        }
        System.out.println("TOTAL----------------------------------------------->"+ importeTotal);
    }

    /**
     * Genera un codigo aleatorio entre 0 y 1 para el pedido
     */
    public void codigoPago(){
        this.codigo= Math.random();
    }



}
