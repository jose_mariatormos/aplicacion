public class PasarelaDePago {

    double importe;
    String codigoPago;
    Pedido pedido;


    public PasarelaDePago(){
    }

    public PasarelaDePago(Pedido pedido){
        this.pedido=pedido;
        this.importe=pedido.importeTotal;
    }


    public void efectivo(){

        System.out.println("Realizando el pago en efectivo..");
        System.out.println("Pagado");

        pedido.codigoPago();

    }

    public void tarjeta(String tarjeta){


        System.out.println("Realizando el pago con tarjeta de credito");
        System.out.println("Pagado");

        pedido.codigoPago();

    }

    public void cuentaBanco(String cuenta){


        System.out.println("Realizando el pago con cuenta bancaria");
        System.out.println("Pagado");

        pedido.codigoPago();


    }


}
