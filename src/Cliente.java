import java.util.Date;

/**
 * Clase Cliente
 *
 * Contiene información de cada cliente
 *
 * @author XG3T2
 * @version 1.0
 * @since 28/02/2022
 */


public class Cliente {

    /**
     * Nombre del cliente
     */
    String nombre;
    /**
     * Apellidos del cliente
     */
    String apellidos;
    /**
     * Fecha de alta del cliente
     */
    Date alta = new Date();
    /**
     * Numero de telefono del cliente
     */
    int telefono;
    /**
     * Direccion del cliente
     */
    String direccion;

    /**
     * Historial de compras del cliente
     */
    String historial;
    Pedido pedido;

    //CONSTRUCTORES

    /**
     * Constructo por defecto
     */
    public Cliente() {
        nombre="";
        apellidos="";
        alta=new Date();
        telefono=123456789;
        direccion="";
        historial="";
    }


    /**
     *
     * @param nombre nombre del cliente
     * @param apellidos apellidos del cliente
     * @param telefono numero de telefono del cliente
     * @param direccion nombre de la direccion del cliente
     */
    public Cliente(String nombre,String apellidos,int telefono,String direccion){

        this.nombre=nombre.toLowerCase();
        this.apellidos=apellidos.toUpperCase();
        this.alta=new Date();
        this.telefono=telefono;
        this.direccion=direccion;
    }


    //METODOS

    /**
     * Metodo que añade codigos al historial del cliente
     * @param pedido
     */
    public void historial(Pedido pedido){
        this.historial=this.historial+","+pedido.codigo;
    }


}