/**
 * Clase para la creacion de productos para los pedidos
 * @author XG3T2
 * @version 1.0
 * @since 28/02/2016
 */



public class Producto {
    String nombre;
    double precio;
    int cantidad;

    //Contructores

    /**
     * Constructor por defecto
     */
    public Producto(){}

    /**
     * Constructor con 3 parametros
     * @param nombre nombre de producto
     * @param precio precio del producto
     * @param cantidad cantidad disponible del producto
     */

    public Producto(String nombre,double precio,int cantidad){
        this.nombre=nombre.toUpperCase();
        this.precio=precio;
        this.cantidad=cantidad;
    }
}

