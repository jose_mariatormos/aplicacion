
import java.util.Scanner;

public class MenuPrincipal {
    public static void main(String[] args) {
        int eleccion=0;
        int cantidad=0;
        Cliente cliente=null;
        Pedido pedido=null;
        Producto producto=null;
        Producto producto2=null;
        String nombre="";
        String apellidos="";
        int telefono = 0;
        String direccion="";
        double precio;
        int error = 0;
        Scanner sc = new Scanner(System.in);

        do {                                                                                                              // MENU PRINCIPAL
            System.out.println("1.Crear cliente");
            System.out.println("2.Crear productos");
            System.out.println("3.Crear pedido");
            System.out.println("4.Eliminar producto");
            System.out.println("5.Resumen del pedido");
            System.out.println("6.Salir");
            eleccion = sc.nextInt();
            sc.nextLine();

            switch (eleccion) {
                case 1:                                                                                                  // CREACION DE CLIENTE
                    error=0;
                    System.out.println("Creacion de cliente");
                    System.out.println("Introduzca su nombre");
                    nombre = sc.nextLine();
                    System.out.println("Introduzca sus apellidos");
                    apellidos = sc.nextLine();

                    do {
                        System.out.println("Introduzca su numero");
                        System.out.println("1.Telefono Fijo");
                        System.out.println("2.Telefono Movil");
                        eleccion = sc.nextInt();
                        switch (eleccion) {
                            case 1:
                                System.out.println("Fijo");
                                do {
                                    telefono = sc.nextInt();

                                    if (Integer.toString(telefono).length() != 9) {
                                        System.out.println("Has  introducido un numero de longitud incorrecta");
                                    } else {

                                        if (Integer.toString(telefono).charAt(0) != '9' && Integer.toString(telefono).charAt(0) != '8') {
                                            System.out.println("El numero introduccido no es un numemro de telefono fijo");
                                        } else {
                                            error++;
                                        }
                                    }
                                } while (error == 0);

                                break;
                            case 2:
                                System.out.println("Movil");

                                do {
                                    telefono = sc.nextInt();

                                    if (Integer.toString(telefono).length() != 9) {
                                        System.out.println("Has  introducido un numero de longitud incorrecta");
                                    } else {
                                        if (Integer.toString(telefono).charAt(0) != '6' && Integer.toString(telefono).charAt(0) != '7') {
                                            System.out.println("El numero introduccido no es un numemro de telefono movil");
                                        } else {
                                            error++;
                                        }
                                    }
                                } while (error == 0);
                                break;
                            default:
                                System.out.println("Opcion invalida");
                        }
                    }while (error==0);

                    System.out.println("Escriba ahora su direccion personal");
                    sc.nextLine();
                    direccion = sc.nextLine();
                    cliente = new Cliente(nombre, apellidos, telefono, direccion);
                    break;




                case 2:                                                                                                 //CREACION DE LOS PRODUCTOS

                    System.out.println("Creacion de primer producto\n");
                    System.out.println("Introduzca nombre del producto");
                    nombre=sc.nextLine();
                    System.out.println("Introduzca precio del producto");
                    precio=sc.nextDouble();
                    System.out.println("Introduzca cantidad del producto");
                    cantidad= sc.nextInt();
                    producto=new Producto(nombre,precio,cantidad);


                    System.out.println("Desea añadir un segundo producto    Y/N");

                    char respuesta = sc.next().charAt(0);

                    if (respuesta=='y' || respuesta=='Y') {
                        System.out.println("Creacion de segundo producto");
                        System.out.println("Introduzca nombre del producto");
                        sc.nextLine();
                        nombre = sc.nextLine();

                        System.out.println("Introduzca precio del producto");
                        precio = sc.nextDouble();

                        System.out.println("Introduzca cantidad del producto");

                        cantidad= sc.nextInt();

                        producto2 = new Producto(nombre, precio,cantidad);

                    }
                    break;


                case 3:                                                                                                 //CREACION DEL PEDIDO

                    if (cliente!=null && producto !=null) {
                        pedido = new Pedido();

                        System.out.println("Creacion del pedido");

                        System.out.println("Pedido creado\n");


                        pedido.agregarCliente(cliente);


                        pedido.agregarProducto(producto);

                        if (producto2 !=null) {
                            pedido.agregarProducto2(producto2);
                        }
                    }
                    else {
                        System.out.println("Falta por crear un cliente o producto para poder realizar un pedido");
                    }
                    break;


                case 4:                                                                                                 //ELIMINACION  DE PRODUCTOS
                    if (pedido!=null) {
                        do {
                            System.out.println("Deseas eliminar algun producto");
                            System.out.println("1.Eliminar producto");
                            System.out.println("2.Eliminar producto2");
                            System.out.println("3.Salir");

                            eleccion = sc.nextInt();

                            switch (eleccion) {

                                case 1:
                                        if (pedido.producto != null) {
                                            pedido.eliminarProducto();
                                        } else {
                                            System.out.println("el producto no existe o ya fue borrado");
                                        }
                                    break;

                                case 2:
                                    if (pedido.producto2 != null) {
                                        pedido.eliminarProducto2();
                                    } else {
                                        System.out.println("el producto no existe o ya fue borrado");
                                    }
                                    break;

                                case 3:
                                    System.out.println("Saliendo\n");
                                    break;
                            }
                        } while (eleccion != 3);
                    }
                    else {
                        System.out.println("No existe un pedido aun");
                    }
                    break;



                case 5:                                                                                                  //RESUMEN DE PEDIDO
                    if (pedido!=null) {
                        System.out.println("Mostrando resumen de la compra\n");
                        pedido.mostrarLista();
                    }
                    else {
                        System.out.println("No existe un pedido aun");
                    }
                    break;


                case 6:                                                                                                 //SALIR
                    System.out.println("Saliendo...");
                    break;

                default:
                    System.out.println("Opcion incorrecta\n");

                    break;
            }
        } while(eleccion!=6);
    }
}

